//
// Created by Viktor Kolpakov on 10.11.2022.
//
#include <iostream>
#include <vector>
#include "overloadings.h"
#include "lint.h"
using namespace std;

//    static Multer *multer;
//    string s_num;
//
//    vector<int> digits;

Lint::Lint(){ }

Lint::Lint(string num): s_num(num){
        for (int i = 0; i < s_num.size(); ++i) {
            digits.push_back(s_num[i] - '0');
        }
    }

    void Lint::setMultMode(Multer *newMulter){
        multer = newMulter;
    }

void Lint::setPrimeMode(Primer *newPrimer){
    primer = newPrimer;
}

Lint Lint::operator* (Lint other){
        Lint res = multer->multiply(digits, other.digits);
        return res;
    }

bool Lint::is_prime (){
    bool res = primer->prime(digits);
    return res;
}

ostream& operator<<(ostream &stream, Lint a){
    cout << a.digits;
    return stream;
}