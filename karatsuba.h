#include <vector>
#include "lint.h"
#include "overloadings.h"
using namespace std;


class Karatsuba : public Multer{

public:
    vector<vector<int>> split_at(vector<int> vec, int m2);
    vector<int> karatsuba(vector<int> digits_a, vector<int> digits_b);

    Lint multiply(vector<int> digits_a, vector<int> digits_b);
};
