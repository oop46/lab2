#include <iostream>
#include <vector>

using namespace std;
class Lint;

class Multer{
public:
    virtual Lint multiply(vector<int> a, vector<int> b) = 0;
};