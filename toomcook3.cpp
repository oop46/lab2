#include "toomcook3.h"

vector<vector<int>> ToomCook3::split(vector<int> vec, int i){
    int size = vec.size();
    if(size < i){
        vector<vector<int>> res;
        vector<int> a2 = {0};
        vector<int> a1 = {0};
        vector<int> a0 = vec;

        res = {a2, a1, a0};
        return res;
    }

    vector<vector<int>> res;
    vector<int> a2 = {};
    vector<int> a1 = {};
    vector<int> a0 = {};

    int counter = 0;
    for (int j = vec.size()-1; j >= 0 ; --j) {
        if(counter/i == 0){
            a0.push_back(vec[j]);
        }else if(counter/i == 1){
            a1.push_back(vec[j]);
        }else if(counter/i == 2){
            a2.push_back(vec[j]);
        }
        counter = counter + 1;
    }

    if(a2.size() == 0){
        a2.push_back(0);
    }
    if(a1.size() == 0){
        a1.push_back(0);
    }
    res = {reverseVector(a2), reverseVector(a1), reverseVector(a0)};
    return  res;
}

int ToomCook3::choose_base(vector<int> a, vector<int> b){
    double r = 3.0;
    double n = a.size() > b.size() ? a.size() : b.size();
    return ceil(n/r);
}

vector<int> ToomCook3::toomcook3(vector<int> a, vector<int> b) {
    if(a.size() == 2 || b.size() == 2){
        return a*b;
    }
    if(a.size() == 1 && b.size() == 1){
        int mult = a[0] * b[0];
        if(mult >=  10){
            vector<int> res = {mult/10, mult%10};
            return res;
        }
        vector<int> res = {mult};
        return res;
    }else if (a.size() == 1 && b.size() > 1){
        return a*b;
    }else if (a.size() > 1 && b.size() == 1){
        vector<int> res = a*b;
        return res;
    }

    int i = choose_base(a, b);
    vector<vector<int>> m_in_pieces = split(a, i);
    vector<int> m2 = m_in_pieces[0];
    vector<int> m1 = m_in_pieces[1];
    vector<int> m0 = m_in_pieces[2];

    vector<vector<int>> n_in_pieces = split(b, i);
    vector<int> n2 = n_in_pieces[0];
    vector<int> n1 = n_in_pieces[1];
    vector<int> n0 = n_in_pieces[2];

    vector<int> p0 = m0;
    vector<int> p1 = m0 + m1 + m2;
    vector<int> p_1 = m0 - m1 + m2;
    vector<int> p_2 = m0 - m1*2 + m2*4;
    vector<int> p_inf = m2;

    vector<int> q0 = n0;
    vector<int> q1 = n0 +n1 + n2;
    vector<int> q_1 = n0 - n1 + n2;
    vector<int> q_2 = n0 - n1*2 + n2*4;
    vector<int> q_inf = n2;

    vector<int> r0 = toomcook3(p0, q0);
    vector<int> r1 = toomcook3(p1, q1);
    vector<int> r_1 = toomcook3(p_1, q_1);
    vector<int> r_2 = toomcook3(p_2, q_2);
    vector<int> r_inf = toomcook3(p_inf, q_inf);


    vector<int> c0 = r0;
    vector<int> c4 = r_inf;
    vector<int> c3 = (r_2 - r1)/3;
    vector<int> c1 = (r1-r_1)/2;
    vector<int> c2 = r_1-r0;
    c3 = (c2 - c3)/2 + r_inf*2;
    c2 = c2 + c1 - c4;
    c1 = c1-c3;

    vector<int> u1 = c1*pow(10, i);
    vector<int> u2 = c2*pow(10, i*2);
    vector<int> u3 = c3*pow(10, i*3);
    vector<int> u4 = c4*pow(10, i*4);

    vector<int> res = c0+u1+u2+u3+u4;
    return res;
}

Lint ToomCook3::multiply(vector<int> digits_a, vector<int> digits_b){
    Lint lint_res;
    vector<int> res = toomcook3(digits_a, digits_b);
    lint_res.digits = res;

    return lint_res;
}
