
#include <iostream>
#include <vector>
#include <cmath>
#include "lint.h"
#include "overloadings.h"
using namespace std;


class Fermat: public Primer{
public:
    vector<int> from_int_to_vector(int a);

    bool prime(vector<int> a);

};