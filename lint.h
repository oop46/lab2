#pragma once // designed to cause the current source file to be included only once in a single compilation.
#include <iostream>
#include <vector>
#include "prime.h"
#include "multer.h"

using namespace std;

class Lint{
    static Multer *multer;
    static Primer *primer;
    string s_num;
public:
    vector<int> digits;

    Lint();

    Lint(string num);

    static void setMultMode(Multer *newMulter);

    static void setPrimeMode(Primer *newPrimer);

    Lint operator* (Lint other);

    bool is_prime();
};

ostream& operator<<(ostream &stream, Lint a);