//
// Created by Viktor Kolpakov on 15.11.2022.
//

#include "miller_rabin_prime_test.h"

int Miller_Rabin::vector_to_int(vector<int> a){
    int num = 0; // vector -> int
    for (int i = 0; i < a.size(); ++i) {
        num = num + a[i] * pow(10, a.size() - i - 1);
    }
    return num;
}

bool Miller_Rabin::prime(vector<int> n){
    int m = vector_to_int(n);
    cout << "Miller Rabin = " << m << endl;
    return 1;
}
