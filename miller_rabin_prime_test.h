//
// Created by Viktor Kolpakov on 15.11.2022.
//
#include <iostream>
#include <vector>
#include <cmath>
#include "lint.h"
#include "overloadings.h"
using namespace std;


class Miller_Rabin: public Primer{
public:
    int vector_to_int(vector<int> a);

    bool prime(vector<int> a);

};