#include <iostream>
#include <vector>
#include <cmath>
#include "overloadings.h"
#include "fermat.h"
using namespace std;


vector<int> Fermat::from_int_to_vector(int a){
    vector<int> res = {};
    for (int i = 0; a != 0; ++i) {
        int digit = a % (int)pow(10, 1);
        a = (a - digit)/10;
        res.push_back(digit);
    }
    return res;
}

bool Fermat::prime(vector<int> n){
    srand(time(0));
    vector<int> a  = from_int_to_vector(rand()) % n;
    vector<int> one = {1};
    vector<int> zero = {0};
    while(true){
        if(a%(n-one) != zero){
            if (pow(a, n-one)%n != one){
                return false;
            }
            break;
        }
        a  = from_int_to_vector(rand()) % n;
    }
    return true;
}