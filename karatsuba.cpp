#include "karatsuba.h"

vector<vector<int>> Karatsuba::split_at(vector<int> vec, int m2){
    int size = vec.size();
    vector<vector<int>> res;
    vector<int> high;
    vector<int> low;

    for (int i = 0; i < vec.size(); ++i) {
        if(i < size - m2){
            high.push_back(vec[i]);
        }else{
            low.push_back(vec[i]);
        }
    }
    res = {high, low};
    return  res;
}

//Lint* Karatsuba::multiply(vector<int> digits_a, vector<int> digits_b) {
//    cout << "karatsuba multiply" << endl;
//    if(digits_a.size() == 1 && digits_b.size() == 1){
//        int mult = digits_a[0] * digits_b[0];
//        if(mult >=  10){
//            vector<int> res = {mult/10, mult%10};
//            Lint* lint_res;
//            lint_res->digits = res;
//            return lint_res;
//        }
//        vector<int> res = {mult};
//        Lint* lint_res;
//        lint_res->digits = res;
//        return lint_res;
//    }else if (digits_a.size() == 1 && digits_b.size() > 1){
//        Lint* lint_res;
//        lint_res->digits = digits_a*digits_b;
//        return lint_res;
//    }else if (digits_a.size() > 1 && digits_b.size() == 1){
//        vector<int> res = digits_a*digits_b;
//        Lint* lint_res;
//        lint_res->digits = res;
//        return lint_res;
//    }
//
//    int m = digits_a.size() < digits_b.size() ? digits_a.size() : digits_b.size();
//    int m2 = m/2;
//
//    vector<int> high1 = split_at(digits_a, m2)[0];
//    vector<int> low1 = split_at(digits_a, m2)[1];
//    vector<int> high2 = split_at(digits_b, m2)[0];
//    vector<int> low2 = split_at(digits_b, m2)[1];
//
//    Lint* z0 = multiply(low1, low2);
//    Lint* z2 = multiply(high1, high2);
//    Lint* z1;
//    z1->digits = multiply(high1+low1, high2+low2)->digits - z2->digits - z0->digits;
//
//    vector<int> res = z2->digits * pow(10, m2 * 2) + z1->digits * pow(10, m2) + z0->digits;
//
//    Lint* lint_res;
//    lint_res->digits = res;
//
//    return lint_res;
//}

vector<int>  Karatsuba::karatsuba(vector<int> digits_a, vector<int> digits_b) {

    if(digits_a.size() == 1 && digits_b.size() == 1){
        int mult = digits_a[0] * digits_b[0];
        if(mult >=  10){
            vector<int> res = {mult/10, mult%10};
            return res;
        }
        vector<int> res = {mult};
        return res;
    }else if (digits_a.size() == 1 && digits_b.size() > 1){
        return digits_a*digits_b;
    }else if (digits_a.size() > 1 && digits_b.size() == 1){
        vector<int> res = digits_a*digits_b;
        return res;
    }

    int m = digits_a.size() < digits_b.size() ? digits_a.size() : digits_b.size();
    int m2 = m/2;

    vector<int> high1 = split_at(digits_a, m2)[0];
    vector<int> low1 = split_at(digits_a, m2)[1];
    vector<int> high2 = split_at(digits_b, m2)[0];
    vector<int> low2 = split_at(digits_b, m2)[1];

    vector<int> z0 = karatsuba(low1, low2);
    vector<int> z2 = karatsuba(high1, high2);
    vector<int> z1 = karatsuba(high1+low1, high2+low2) - z2 - z0;

    vector<int> res = z2 * pow(10, m2 * 2) + z1 * pow(10, m2) + z0;

    return res;
}

Lint Karatsuba::multiply(vector<int> digits_a, vector<int> digits_b){
    Lint lint_res;
    vector<int> res = karatsuba(digits_a, digits_b);
    lint_res.digits = res;

    return lint_res;
}