#include <iostream>
#include <vector>
#include <cmath>
#include "lint.h"
#include "overloadings.h"
using namespace std;


class ToomCook3 : public Multer{
public:
    vector<vector<int>> split(vector<int> vec, int i);

    int choose_base(vector<int> a, vector<int> b);

    vector<int> toomcook3(vector<int> a, vector<int> b);

    Lint multiply(vector<int> a, vector<int> b);
};