#include <iostream>
#include <vector>
using namespace std;

class Primer{
public:
    virtual bool prime(vector<int> a) = 0;
};
