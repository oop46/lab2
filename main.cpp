#include <iostream>
#include <vector>
#include "overloadings.h"
#include "karatsuba.h"
#include "toomcook3.h"
#include "fermat.h"
#include "miller_rabin_prime_test.h"

using namespace std;

class basic : public Multer{
public:
    Lint multiply(vector<int> a, vector<int> b) {
        cout << "Multiplication is not set" << endl << "All multiplications will be equal 1";
        vector<int> res = {1};
        Lint lint_res;
        lint_res.digits = res;
        return lint_res;
    }
};

class basic_prime : public Primer{
public:
    bool prime(vector<int> a){
        cout << "Prime mode is not set" << endl << "All will be equal 0";
        return 0;
    }
};

// we have to initialize multer, before we can assign any value to multer, because of it is static
basic b;
Multer* Lint::multer = &b;

basic_prime p;
Primer* Lint::primer = &p;

int main() {
    Lint a("11"), b("456");

//    select multiplication mode
    Lint::setMultMode(new Karatsuba());
    //Lint::setMultMode(new ToomCook3());
//    select prime test mode
//    Lint::setPrimeMode(new Fermat());
    Lint::setPrimeMode(new Miller_Rabin());
    cout << a.is_prime() << endl;
    Lint c = a*b;
    cout << c << endl;

    vector<int> w = a.digits;
    vector<int> q = b.digits;
    cout << w*q << endl;

    return 0;
}
